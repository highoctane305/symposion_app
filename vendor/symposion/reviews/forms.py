import bleach
from django import forms
from django.utils.translation import ugettext_lazy as _

from symposion.reviews.models import Review, Comment, ProposalMessage, VOTES


class ReviewForm(forms.ModelForm):

    required_css_class = 'label-required'

    class Meta:
        model = Review
        fields = ["vote", "comment"]

    def __init__(self, *args, **kwargs):
        super(ReviewForm, self).__init__(*args, **kwargs)
        self.fields["vote"] = forms.ChoiceField(
            widget=forms.RadioSelect(),
            choices=VOTES.CHOICES
        )

    def clean_comment(self):
        comment = self.cleaned_data.get('comment')
        cleaned_comment = bleach.clean(comment)
        return cleaned_comment


class ReviewCommentForm(forms.ModelForm):

    required_css_class = 'label-required'

    class Meta:
        model = Comment
        fields = ["text"]

    def clean_text(self):
        text = self.cleaned_data.get('text')
        cleaned_text = bleach.clean(text)
        return cleaned_text


class SpeakerCommentForm(forms.ModelForm):

    required_css_class = 'label-required'

    class Meta:
        model = ProposalMessage
        fields = ["message"]

    def clean_message(self):
        message = self.cleaned_data.get('message')
        cleaned_message = bleach.clean(message)
        return cleaned_message


class BulkPresentationForm(forms.Form):

    required_css_class = 'label-required'

    status = forms.ChoiceField(
        choices=(
            ('accepted', 'accepted'),
            ('rejected', 'rejected'),
            ('undecided', 'undecided'),
            ('standby', 'standby')
        ),
        label="Set status to:",
        help_text="Status to apply to the listed talk ids"
    )
    talk_ids = forms.CharField(
        label=_("Talk ids"),
        max_length=500,
        help_text=_("Provide a comma seperated list of talk ids to update.")
    )
